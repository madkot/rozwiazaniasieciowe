public class Firma {
    //nazwie firmy, adresie, numerze telefonu, adresie email, stronie www.
    private String nazwa;
    private String telefon;
    private String adres;
    private String email;
    private String strona;


    public Firma(String nazwa, String telefon, String adres, String email, String strona) {
        this.nazwa = nazwa;
        this.telefon = telefon;
        this.adres = adres;
        this.email = email;
        this.strona = strona;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStrona() {
        return strona;
    }

    public void setStrona(String strona) {
        this.strona = strona;
    }

    @Override
    public String toString() {
        return "" + nazwa + '\n' +
                "" + telefon + '\n' +
                "" + adres + '\n' +
                "" + email + '\n' +
                "" + strona + '\n';
    }
}
