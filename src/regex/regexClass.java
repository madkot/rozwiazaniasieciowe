package regex;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class regexClass {
    public static String getText(String url) throws Exception {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;
        String newLine = System.getProperty("line.separator");
        while ((inputLine = in.readLine()) != null)
            response.append(inputLine + newLine);

        in.close();

        return response.toString();
    }
    public static String getName(String a) throws Exception {
        Pattern nazwaFirmyPattern = Pattern.compile("<h3><a(.*?)>(.*?)</a></h3>");
        Matcher nazwaFirmyContent = nazwaFirmyPattern.matcher(a);
        if (nazwaFirmyContent.find()) {
            return nazwaFirmyContent.group(2);
        }else{
            return null;
        }
    }
    public static void main(String[] args) throws Exception{
        String content = regexClass.getText("http://kot.dev/page2.html");
        String pattern = "<div class=\"paginator\">(.*?)</div>";
        //Firma firmaCol = new Firma("Nazwa","12313213","Aset asfdas fds","kot@veng.pl","www.veng.pl");

        Pattern pagerPattern = Pattern.compile(pattern);
        Matcher pagerContent = pagerPattern.matcher(content);
        if (pagerContent.find( )) {
            System.out.println("Found value: " + pagerContent.group(1) );
            //Pobieranie linkow z paginacji
            /*
            Pattern linksPattern = Pattern.compile("<a href=\"(.*?)\">([0-9])</a>");
            Matcher linksContent = linksPattern.matcher(pagerContent.group(1));
            int count = 0;

            while(linksContent.find()) {
                count++;
                System.out.println("Found: "+linksContent.group(1));
            }
            */
            //koniec pobeirania linkow z paginacji

        }else {
            System.out.println("NO MATCH");
        }
        //Pobieranie firm
        Pattern firmaPattern = Pattern.compile("<div id=\"firmy_m\"><ol>([\\s\\S]*?)</ol>");
        Matcher firmaContent = firmaPattern.matcher(content);
        if (firmaContent.find( )) {
            // nazwie firmy, adresie, numerze telefonu, adresie email, stronie www.
            System.out.println("Found-: " + firmaContent.group(1));
            //Mamy kod z danymi jednej firmy
            //Pobieramy nazwe
            String nazwa = regexClass.getName(firmaContent.group(1));
            System.out.println(nazwa);
            /** Nazwa zdobyta */
            Pattern nazwaFirmyPattern = Pattern.compile("<div class=\"adres\">([\\s\\S]*?)<span class=\"opis\">");
            Matcher nazwaFirmyContent = nazwaFirmyPattern.matcher(content);
            String daneFirmy = "";
            if (nazwaFirmyContent.find()) {
                daneFirmy = nazwaFirmyContent.group(1);
            }else{
                System.out.println("Nie znaleziono elementu z danymi firmy");
            }
            // [\d ]* -kombinacje cyfr i spacji ([0-9\-\+ ]{9,15})
            //String temp1 = temp.replaceAll("\\(0-([0-9]{2})\\)", "$1");
            //(\d{2,4} \d{2,4} \d{2,4}|\d{2,4} \d{2,4} \d{2,4}  \d{2,4})

            Pattern telefonPattern = Pattern.compile("([0-9+ ]{6,15})</p>");
            Matcher telefonContent = telefonPattern.matcher(daneFirmy);
            String tel = "";
            while(telefonContent.find()) {
                System.out.println("Tel: "+telefonContent.group(1));
                tel = tel + "\n" + telefonContent.group(1);
            }
            Pattern mailPattern = Pattern.compile("([a-z0-9_\\.-]+)@([\\da-z\\.-]+)\\.([a-z\\.]{2,6})");
            Matcher mailContent = mailPattern.matcher(daneFirmy);
            if(mailContent.find()) {
                System.out.println("mail: "+mailContent.group(0));
                String mail = mailContent.group(0);
            }
            Pattern stronaPattern = Pattern.compile("(https?:\\/\\/)([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?");
            Matcher stronaContent = stronaPattern.matcher(daneFirmy);
            if(stronaContent.find()) {
                System.out.println("strona: "+stronaContent.group(0));
                String strona = stronaContent.group(0);
            }
            Pattern addresPattern = Pattern.compile("<p>([a-zA-Z0-9\\s\\.\\-ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*)</p>");
            Matcher addresContent = addresPattern.matcher(daneFirmy);
            String adres = "";
            while(addresContent.find()) {
                //System.out.println("adres: "+addresContent.group(1));
                adres = adres + "\n" + addresContent.group(1);
            }
            System.out.println(adres);
            //List<Firma> firma = new ArrayList<Firma>();
            //firma.add(nazwa);

        }else {
            System.out.println("Nie znaleziono danych o firmach na tej stronie.");
        }
        //System.out.println(content);
    }
}
