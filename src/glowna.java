import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class glowna {
    public static String getText(String url) throws Exception {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;
        String newLine = System.getProperty("line.separator");
        while ((inputLine = in.readLine()) != null)
            response.append(inputLine + newLine);

        in.close();

        return response.toString();
    }
    public static String getName(String a) throws Exception {
        Pattern nazwaFirmyPattern = Pattern.compile("<h3><a(.*?)>(.*?)</a></h3>");
        Matcher nazwaFirmyContent = nazwaFirmyPattern.matcher(a);
        if (nazwaFirmyContent.find()) {
            return nazwaFirmyContent.group(2);
        }else{
            return null;
        }
    }
    public static void main(String[] args) throws Exception{
        String content = glowna.getText("http://www.info-net.com.pl/katalog/zielona-gora/informatyka-telekomunikacja/informatyka-internet--uslugi");
        String pattern = "<div class=\"paginator\">(.*?)</div>";
        String homeLink = "http://www.info-net.com.pl/";
        String daneFirmy = "";
        String nazwa = "";
        List<Firma> firmaCol = new ArrayList<Firma>();
        //String adresy[] = new String[99];
        String[] anArray = new String[10];
        anArray[0] = "http://www.info-net.com.pl/katalog/zielona-gora/informatyka-telekomunikacja/informatyka-internet--uslugi";

        Pattern pagerPattern = Pattern.compile(pattern);
        Matcher pagerContent = pagerPattern.matcher(content);
        if (pagerContent.find( )) {
            //System.out.println("Found value: " + pagerContent.group(1) );
            //Pobieranie linkow z paginacji

            Pattern linksPattern = Pattern.compile("<a href=\"(.*?)\">([0-9])</a>");
            Matcher linksContent = linksPattern.matcher(pagerContent.group(1));
            int count = 0;

            while(linksContent.find()) {
                count++;
                anArray[count] = homeLink + linksContent.group(1);
            }
            for ( int i=0;i<anArray.length;i++) {
                if(anArray[i] != null) {
                    System.out.println(anArray[i]);
                }
            }

            //koniec pobeirania linkow z paginacji

        }else {
            System.out.println("NO MATCH");
        }
        //Pobieranie firm
        for ( int i=0;i<anArray.length;i++) {
            if(anArray[i] != null) {
                content = glowna.getText(anArray[i]);

        Pattern firmaPattern = Pattern.compile("<div id=\"firmy_m\"><ol>([\\s\\S]*?)</ol>");
        Matcher firmaContent = firmaPattern.matcher(content);
        while (firmaContent.find( )) {
            // nazwie firmy, adresie, numerze telefonu, adresie email, stronie www.
            //System.out.println("Found-: " + firmaContent.group(1));
            //Mamy kod z danymi jednej firmy
            //Pobieramy nazwe
            nazwa = glowna.getName(firmaContent.group(1));
            /** Nazwa zdobyta */
            Pattern daneFirmyPattern = Pattern.compile("<div class=\"adres\">([\\s\\S]*?)<div style=\"float:left;clear:both;\">");
            Matcher daneFirmyContent = daneFirmyPattern.matcher(firmaContent.group(1));

            if (daneFirmyContent.find()) {
                daneFirmy = daneFirmyContent.group(1);
            }else{
                System.out.println("Nie znaleziono elementu z danymi firmy");
            }
            //"(\\d{2} \\d{3} \\d{2} \\d{2}|\\d{3} \\d{3} \\d{3}|\\d{3} \\d{2} \\d{2}|\\d{3} \\d{4} \\d{3})";
            // Telefon:
            Pattern telefonPattern = Pattern.compile("  ([0-9+ ]{6,15})</p>");
            Matcher telefonContent = telefonPattern.matcher(daneFirmy);
            String tel = "";
            int count = 1;
            while(telefonContent.find()) {
                if (count == 1) tel = telefonContent.group(1);
                else tel = tel + "\n" + telefonContent.group(1);
                count++;
            }
            // Adres e-mail
            Pattern mailPattern = Pattern.compile("([a-z0-9_\\.-]+)@([\\da-z\\.-]+)\\.([a-z\\.]{2,6})");
            Matcher mailContent = mailPattern.matcher(daneFirmy);
            String mail ="";
            if(mailContent.find())
                mail = mailContent.group(0);

            // Strona www:
            Pattern stronaPattern = Pattern.compile("(https?:\\/\\/)([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?");
            Matcher stronaContent = stronaPattern.matcher(daneFirmy);
            String strona ="";
            if(stronaContent.find())
                strona = stronaContent.group(0);

            // Adres:
            Pattern addresPattern = Pattern.compile("<p>([a-zA-Z0-9\\s\\.\\-ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]*)</p>");
            Matcher addresContent = addresPattern.matcher(daneFirmy);
            String adres = "";

            count = 1;
            while(addresContent.find()) {
                if (count == 1) adres = addresContent.group(1);
                else adres = adres + "\n" + addresContent.group(1);
                count++;
            }
            //dodanie danych Firmy do kolekcji
            firmaCol.add(new Firma(nazwa,tel,adres,mail,strona));


        }//endwhile
            }
        }
        for(int i=0 ; i<firmaCol.size() ; i++)
            System.out.println(firmaCol.get(i).toString());
    }
}
